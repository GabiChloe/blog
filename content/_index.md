## Welkom op mijn blog!
Welkom op mijn blog!
In Design Challenge 4 ben ik samen met mijn team aan de slag gegaan met het volgende concept:

De jongeren willen we in beweging laten komen door ze gezamenlijk een spel te laten spelen door de gehele wijk Carnisse. Wat wij willen bereiken met het spel is dat jongeren teams gaan vormen wat bijdraagt aan de samenwerking, communicatie en samenhorigheid. Het spel zet de jongeren aan om actief het spel te gaan spelen en een strategie te bedenken als team. Dit draagt allemaal bij aan een prettige leefomgeving, omdat de jongeren hun sociale vaardigheden in zetten en verbeteren om tot een zo hoog mogelijke score te komen. 
