---
title: About me
subtitle: Welkom op mijn blog
comments: false
---

  
Welkom, ik ben Gabi Broodman en studeer 'Communication and Multimedia design' in Rotterdam.

In mijn blog kun je wekelijks lezen wat ik maak en leer op school. Door mijn blog te volgen kun je mijn leerproces zien tijdens mijn studie. Ik beschrijf in mijn blog mijn plan van aanpak, ontwerpprocessen en laat het eindresultaat zien. 

# Doel
Ik hoop binenn vier jaar mijn Hbo diploma te halen en aan de slag te gaan in het bedrijfsleven. Ik ben nog zoekende naar welke functie ik bij me zelf vind passen. Het liefst zou ik een leidinggevende functie willen hebben binnen een bedrijf, organisatie of stichting. Mijn passie ligt voornamelijk in de zorg en het onderwijs. 

# Vooropleidingen

Grafisch Lyceum Rotterdam              
In 2017 ben ik afgestudeerd aan het Grafisch Lyceum Rotterdam. Leerjaar 3 en 4 heb ik in de Masterclass zetten. De Masterclass is één klas op het GLR. Na een strenge selectie ben ik toegelaten. Ik heb mijn opleiding afgerond met een diploma en Masterclass certificaat.

KNSB-opleiding         
De opleiding SL2-langebaan afgerond met een diploma bij de KNSB. Met dit diploma ben ik bevoegd schaatslessen te verzorgen voor jeugd en volwassenen.