---
title: Plan van aanpak en workshop deskresearch
date: 2018-04-24
---
Vandaag heb ik alle onderzoeksresultaten erbij gepakt en ben ik aan de slag gegaan met het maken van een gebruikersprofiel. Ik heb gekeken welke nationaliteiten er veel voorkomen in de wijk, uit welk milieu ze komen, waar ze wonen, wat de interesses zijn enzovoort. Ik kon veel informatie vinden op de website van de gemeente Rotterdam. De gemeente heeft allemaal tabellen en grafieken van de bevolking. Al deze gegevens heb ik verwerkt in een gebruikersprofiel. 

![2018-04-24](gebruikersprofiel.jpg)

# Workshop deskresearch 
Bij de workshop deskresearch heb ik geleerd dat je op Google op verschillende manier kunt zoeken. In de afbeelding zie je wat ik heb geleerd. Ook heb ik inzicht gekregen in de APA-stijl. Ik had nog nooit met deze methode gewerkt. Ik heb gelijk op HINT de handleiding van de APA-stijl gedownload, zodat ik die kan gebruiken bij het verwerken van mijn eigen onderzoek. Tijdens de les heb ik verschillende dingen moeten zoeken op Google, maar ook op een databank. Bij een databank zijn de documenten gescand en weet je dat het goede onderzoeken zijn die je kunt gebruiken 

![2018-04-24](deskresearchl.jpg)
