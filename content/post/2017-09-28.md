---
title: 28 september 2017
date: 2017-09-28
---

Wat doe je op je vrije dag? Spelletjes spelen en er een spelanalyse van maken! Zo heb ik mijn vrije dag besteed. Uit onze short-list kwamen een aantal spellen die relevant zouden kunnen zijn voor ons spel. We hebben de relevante spellen onderverdeelt per teamlid.  Ik had gekozen om de volgende spellen te gaan onderzoeken: Party en Co en Dumb ways to die.

# Party en CO

Als eerste ben ik opzoek gegaan naar het document spelanalyse. Het document heb ik goed doorgelezen en heb deze vervolgens geprint. Dit leek me handig, omdat ik dan tijdens het spelen van het spel kan zien welke vragen ik moet gaan beantwoorden. Een spelanalyse kun je niet maken zonder het spel, dus ben ik op zoek gegaan naar het spel Party en Co. Het was even zoeken, want ik speel het niet dagelijks. ;) Het spel kun je niet in je eentje spelen, dus ik moest opzoek naar gezelschap. En dat is wel lastig als je op dat moment de hele dag alleen thuis bent. Ik heb mijn moeder gebeld en heb gevraagd of ik langs kon komen om samen met mijn moeder en stiefvader het spel te spelen. Ik heb mijn fototoestel en spel gepakt en ben bij mijn moeder langs gegaan. 

Ik heb het spel klaar gelegd op de tafel en heb aan mijn ouders uitgelegd wat de bedoeling was. Mijn moeder en ik heb laten afwisselen met foto’s maken, zodat je goed zag dat het spel daadwerkelijk werd gespeeld. 

![2017-09-28](spelspelen.png)

Tijdens het spelen van het spel heb ik al de vragen die bij een spelanalyse horen kunnen beantwoorden. Ik heb mijn ouders bedankt voor het mee spelen van het spel. Vond het weer grappig om het spel te spelen. 

**Plus- en minpunten**
+ Je bent fysiek bezig tijdens het spel
+ Je leert elkaar op een leuke manier kennen
+ Tijdsdruk door zandloper
– Er kan onduidelijkheid ontstaan bij het goed of fout keuren van de opdracht
– Je moet de zandloper wel goed in de gaten houden, want je hoort niet of de tijd om is.



# Dumb ways to die

Voor het spel Dumb ways to die had ik niet meer voorbereiding nodig. Ik heb de app gedownload op mijn mobiel. En heb de spelanalyse vragen weer naast me neergelegd, zodat ik de vragen kon gaan beantwoorden. Ik heb spel meerdere keren gespeeld. Ik vond het belangrijk om meerdere levels te halen, want dan pas kan je merken wat het effect is. Is het leuk om te spelen? Of wordt het al snel saai? Ik heb zelf veel lol beleefd aan het spelen van het spel. Er zit veel humor is.

![2017-09-28](app.jpg)

Het spel ziet er heel mooi vormgegeven uit en dat spreek mij ook erg aan. De tekenstijl zou ik graag willen meenemen naar het spel wat mij gaan maken. De poppejes ziet er leuk en aandoenlijk uit en je kunt ze ook nog een aanpassen. Je kunt je eigen poppetje samenstellen. 

![2017-09-28](poppetjes.jpg)


**Plus- en minpunten**
+ Afwisselende gamebesturing (telefoon kantelen, swipen, drukken en meer)
+ De game weet op een onopvallende manier toch bewustwording te creëren
+ Humor in het spel
+ Tempo verhoogt na elke drie minigames
+ Variantie in de minigames
+/- Instructies ontbreken, waardoor het soms lastig is om de bediening te snappen
– De verschillende levels staan soms te ver van de boodschap af.
– Het is nutteloos om de poppetjes aan te passen in de game

