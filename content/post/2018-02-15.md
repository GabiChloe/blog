---
title: Teampresentatie maken
date: 2018-02-15
---
Ik heb mijn schetsen erbij gepakt en ik heb die vervolgens in Illustrator nagemaakt. Ik ben begonnen met het maken van de spelkaarten. Ik heb de Styleguide van Wesley erbij gepakt voor de juiste kleursamenstelling. Ik heb de voor-  en achterkant van de spelkaart vormgegeven en doorgestuurd naar Nick. Op deze manier kon hij zien waar ik graag de illustraties wilde hebben. 

Nick is ondertussen aan de slag gegaan met het maken van de illustraties en heeft die naar mij toegestuurd. Ook had hij ze al op de spelkaarten geplaatst en ze wat aangepast. Ik heb vervolgens de informatie voor op de spelkaarten van iedereen verzameld en in Indesign de teksten opgemaakt. Toen de spelkaarten voorzien waren van tekst, heb ik een dia gemaakt waarop de kaarten door elkaar worden geschud. Ik heb tijdens het maken van de presentatie het team op de hoogte gehouden van mijn bezigheden. 

# Ontwerpproceskaart
Ik ben aan de slag gegaan met het maken van de dia’s waarop de Ontwerpproceskaart moest komen. Om deze goed te kunnen maken heb ik de studioplanning erbij gepakt en mijn eerder gemaakte Ontwerpproceskaarten. Ik heb de feedback nog eens doorgelezen en ben vervolgens aan de slag gegaan. Hieronder het resultaat.
![2018-02-15](Ontwerpproceskaart.jpg)

Toen ik al mijn eigen dia’s af had, heb ik de rest van het team er nog even aan herinnerd dat ik graag die dag alle informatie wilde hebben voor in de presentatie. Iedereen heeft netjes zijn document aangeleverd via Google Drive. Ik heb de presentatie vervolgens vormgegeven in Indesign en gedeeld met de groep. 

![2018-02-15](presentatie.jpg)
