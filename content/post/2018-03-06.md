---
title: Begin Lifestyle diary
date: 2018-03-06
---
De voorjaarsvakantie zit erop en gaan weer aan de slag met Design Challenge 3. Vandaag kon ik niet bij mijn projectgroep zijn vanwege een sollicitatiegesprek en het klassenvertegenwoordigersoverleg. Na deze bezigheden heb ik met mijn team de dag nog doorgesproken en opnieuw wat taken verdeeld. Deze week moest er namelijk een Lifestyle diary gemaakt worden van je eigen levensstijl. Ik heb de opdrachtomschrijving doorgelezen en op internet gaan zoeken naar voorbeelden. Ik heb helaas geen creatieve Lifestyle diary’s gevonden en vond het daardoor extra leuk om dit zelf wel voor elkaar te krijgen. Het leek me leuk om aan de slag te gaan met de collagetechniek. Hier heb je veel afbeeldingen voor nodig en daarom ben ik tijdschriften gaan verzamelen bij de Albert Heijn en Dirk. Vervolgens ben ik A2-papier gaan halen in verschillende kleuren. 
