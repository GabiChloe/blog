---
title: 19 oktober 2017
date: 2017-10-19
---

# Visuale borspel

# **STARRT: Samenwerken**       

Vandaag vroeg op gestaan om weer verder te gaan met het bordspel. Iedereen van mijn team had gereageerd op mijn bordspel ontwerp en vonden het goed als ik het netter zou gaan uitwerken. Met netter uitwerken bedoel ik de lijndiktes, illustraties goed uitlijnen, illustraties verfijnder maken en de bomen en wolken wat kleiner maken. 

Ik was nog niet helemaal te vrede over hoe ik de kleuren in het spelbord had toegepast. Daarom ben ik verschillende varianten gaan maken, zodat je kun gaan kiezen. Hieronder zie je wat ik heb gemaakt. 

![2017-10-19](bordspel-3.jpg)

Deze heb ik vervolgens weer naar het team toe gestuurd. De voorkeur ligt nu bij het blauw/groene speelbord. 