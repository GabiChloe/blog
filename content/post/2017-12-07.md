---
title: Testen van concept
Date: 2017-12-07
---
# Testen van concept
Samen met Ricardio en Meintje had ik afgesproken op school om ons gekozen concept te gaan testen. We hadden het prototype op ons mobiel staan, zodat mensen ons idee ook daadwerkelijk konden testen. Voordat we de straat op zijn gegaan hebben we eerst wat interviewvragen bedacht.

![2017-12-07](notitie.jpg)

We hebben de taken verdeeld. Meintje en ik zouden de mensen aanspreken en het geluid opnemen. Ricardio zou de app demonstreren en observeren. Vervolgens zijn we de stad ingegaan om te gaan testen of mensen ons concept wel bruikbaar vonden. We zijn de Markthal ingegaan, omdat het buiten regende en ontzettend koud was. De Markthal leek ons een geschikte plek, omdat mensen dan warm en droog staan en vaak niet veel haast hebben. Al snel bleek onze doelgroep niet echt te vinden te zijn in de Markthal. Er liepen voornamelijk oudere mensen en toeristen. 

We hebben wel een meisje van de Kunstacademie kunnen vragen wat ze van ons concept vond. Ze vond het heel leuk bedacht en denkt zelf dat ze de chat-bot wel zou gebruiken als ze naar een optreden zou gaan. De voorbeeldvragen in de chat-bot vond ze ook heel passend. 

![2017-12-07](schermenchat.jpg)

**Den Haag vs Rotterdam**                     
We merkten een groot verschil tussen de mensen uit Den Haag en Rotterdam. In Den Haag konden we aan iedereen onze vragen stellen. Ze namen de tijd en vonden het leuk om ons te helpen. In Rotterdam had niemand zin om onze vragen te beantwoorden en liepen vaak stug door. Het was grappig om dit verschil zo duidelijk te kunnen merken.

Na weinig succes te hebben gehad in de Markthal zijn we gaan kijken waar we nog meer konden gaan staan. Het weer was te slecht om buiten te gaan staan. We hadden besloten ons concept op school te gaan testen. Daar lopen genoeg mensen uit onze doelgroep rond en nemen mensen misschien wel de tijd om ons te helpen. Eenmaal op school hebben we bij het Lab beneden een aantal studenten aangesproken en nuttige feedback gekregen.

# Testresultaten
De studenten gaven aan weinig tot niks te horen van Paard. Sommige kende Paard niet eens. Ze vonden het fijn om via Social media op de hoogte gehouden te worden van de events. Ze vonden onze chat-bot leuk bedacht en gaven aan dat het kan werken. Een app zouden ze er niet voor downloaden, dus zouden ze het fijner vinden om de berichten via Messenger te ontvangen. Bijna iedereen gaf aan Messenger wel eens te gebruiken. Niet heel vaak, maar meer voor bijzondere/specifieke dingen. Ze zouden niet snel uit zichzelf een eerste berichtje sturen via de chat-bot. Eerst zou de chat-bot hun informatie moeten geven of een vraag moeten stellen. 
