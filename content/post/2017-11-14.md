---
title: Intro Paard/keuzevak
date: 2017-11-14
---
Kwartaal 2 is vandaag van start gegaan in Den Haag bij Poppodium Paard. We hebben te horen gekregen dat we een probleem moeten oplossing voor poppodium Paard. De opdrachtgever is ontwerpbureau Fabrique die sinds een jaar de nieuwe huisstijl van Paard heeft opgezet. 

![2017-11-14](ingang.jpg)

**De onderzoeksvraag is:**                         
Hoe kan Paard vaker van betekenis zijn voor haar bezoekers?

Wij moeten proberen de bezoekers vaker dan één keer terug te laten komen. Bezoekers moeten voortaan een herhaal bezoek plannen. 

# Keuzevak
Vandaag is mijn keuzevak “creëer je eigen merk" van start gegaan op locatie Kralingse Zoom. Ik hoop hier meer de marketing kant van een merk te leren kennen. 