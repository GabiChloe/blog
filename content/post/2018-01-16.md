---
title: Starrt High-fit prototype (11) Expo visuals (10)
date: 2018-01-16
---
# Test hem zelf
Een chat-bot is een geautomatiseerde gesprekspartner. Deze wordt het meest gebruikt via Facebook Messenger.
Een chat-bot, of kort gezegd bot, kan de bezoekers begeleiden voor, na en tijdens hun bezoek aan Paard. Op deze manier zal hun bezoek persoonlijker worden en zullen zij het gevoel krijgen bij Paard te horen. 
![2018-01-16](expo.jpg)

In de chat-bot wilden we niet alleen maar artiesten vermelden. Ik heb daarom nagedacht over wat de gebruiker zou willen vragen aan de chat-bot en in welke fase de gebruiker zich bevindt? Ik heb de gegevens van ons onderzoek gebruikt om de wensen van de doelgroep erin te verwerken

# Validatie 
(gevalideerd door Simone Tertoolen)
![2018-01-16](validatie-1.jpg)


# Expo visuals
# Voorbeeld chat-bot
Om de juiste ondersteunende visuals te maken, heb ik eerst zelf gebruik gemaakt van de chat-bot. Ik heb genoteerd waar ik tegenaan liep en waar ondersteuning nodig was voor een optimale ervaring. Vervolgens heb ik elke vraag gesteld die in de chat-bot geprogrammeerd stond en er een schermafbeelding van gemaakt. In Photoshop heb ik de afbeeldingen bewerkt en er één lopend gesprek van gemaakt. Vervolgens heb ik naast de berichten een tijdlijn gemaakt met voor, tijdens of na het concert. Door de soort vraag die de gebruiker stelt, kun je dan zien in welke fase hij zit. Ook hoeft de gebruiker niet te blijven testen om te kunnen zien wat er allemaal mogelijk is met onze chat-bot.  

![2018-01-16](expo1.jpg)
![2018-01-16](expo2.jpg)
![2018-01-16](expo3.jpg)


# Flyer
Ik heb een flyer gemaakt van het programma overzicht van Paard. Ik heb op de flyer alleen de artiesten en data vermeld die ook in de chat-bot geprogrammeerd staan. De gebruiker kan dan rechtstreeks vanaf de flyer kiezen welke artiest hij invult zonder een fout te begaan. 
Door deze twee visuals toe te voegen aan het high-fit prototype kan ik de gebruiker een optimale ervaring bieden met de chat-bot. 

![2018-01-16](expo4.jpg)

# Analysebord
Toch wilde ik nog wat extra’s bieden om de opdrachtgever te overtuigen van ons concept. Ik heb ervoor gekozen een analysebord te maken. Op het analysebord kun je zien hoeveel mensen al gebruik gemaakt hebben van de chat-bot, wat ze hebben ingevuld en waar ze op hebben geklikt. Dit is bruikbare informatie voor Paard om bijvoorbeeld een artiest te boeken die vaak wordt opgezocht/gevraagd.


![2018-01-16](expo5.jpg)