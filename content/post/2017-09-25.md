---
title: 25 september 2017
date: 2017-09-25
---

# **STARRT: Samenwerken** 

# Gesprek met teamlid

Maandag had ik het hoorcollege Onderzoeken. Daarna heb ik gelijk even het team bij elkaar geroepen. Maarten ging toevallig naar de winkel, dus dat kwam goed uit. Ik heb iedereen zijn verhaal laten vertellen en heb dit in steekwoorden samengevat samen met Savannah. Ik heb daarna nog een langer gesprek gehad met Savannah, omdat ik niet wist dat zei er ook erg mee zat. Het verhaal werd steeds completer en het ligt natuurlijk heel gevoelig om iemand aan te vallen op mijn gedrag. Ik heb er voor gekozen eerst naar Mieke te gaan om te vragen hoe ik dit het beste kon gaan aanpakken. Ze gaf aan dat ze het goed vond dat we deze stap wilde gaan zetten en eerst advies kwamen vragen bij haar. Als tip had ze gegeven niet als team dit onderwerp te bespreken, omdat het dan met z’n alle tegen Maarten zijn. Daar had ze helemaal gelijk in. 

Ik heb zelf niet een hechte band met Maarten waardoor ik het idee had dat hij zich sneller aangevallen zou voelen als ik het tegen hem zou zeggen. Ik heb gevraagd of Savannah bij het gesprek wilde zijn. Savannah gaat wel veel om met Maarten om en dan voelt het meer als een driehoeks verhouding. We hebben Maarten geappt of hij kon komen, omdat we iets met hem wilde bespreken. 

Ik heb het woord gevoerd. Dat was voornamelijk omdat ik de teamcaptain was en ik al wel wat meer ervaring had met mooilijke gesprekken. Savannah vond het te lastig om dit gesprek te houden.

Ik ben begonnen met dat we feedback hebben gekregen als team van Mieke. Dat ik dat had doorgelezen en dat we het best goed hadden gedaan. Ik heb wat punten benoemt en ik merkte aan Maarten dat hij blij was met de goede feedback. Dat vond ik een goede ingang om te beginnen met het lastige gedeelte van het gesprek. Ik heb aangegeven dat de feedback voornamelijk is voor Sem, Tyoni, Savannah en mij en dat hij helaas niks heeft bij gedragen en daardoor ook gen feedback heeft gekregen. Daar was het het mee eens en heb gevraagd hoe het kwam dat hij niet mee doet met het team. Hij heeft wat redenen gegeven en heb daarna onze frustratie punten zo goed en tactisch mogelijk proberen over te brengen aan Maarten. Hij pakte het gelukkig goed op. Dit gaf wel een bepaalde opluchting. Na het gesprek heb ik Sem en Tyoni ingelicht over het gesprek en dat alle goed was gegaan. 